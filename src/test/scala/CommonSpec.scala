import org.scalatest._
import time._

abstract class CommonSpec
    extends wordspec.AnyWordSpec
    with matchers.should.Matchers
    with BeforeAndAfterEach
    with BeforeAndAfterAll
    with concurrent.ScalaFutures {

  var finder: Finder = null
  var longTimeout = timeout(Span(1000, Millis))

  "Базовый случай" ignore {
    finder
      .find("fligdkmtmgdldkgkkfjsivjoqqxasoskvprfejrgkrlidgkdssadkudfglidgkmashdua", "ligdkm")
      .futureValue should be(Seq(FoundSubstring("ligdkm", 1), FoundSubstring("lidgkm", 57)))
  }

  "Очень длинная исходная строка" ignore {
    val complexityN = 1000000

    val sb = new StringBuilder
    (0 until complexityN) foreach { _ =>
      sb.append("fligdkmtmgdldkgkkfjsivjoqqxasoskvprfejrgkrlidgkdssadkudfglidgkmashdua")
    }

    finder.find(sb.toString, "ligdkm").futureValue(longTimeout) should
      be(
        (0 until complexityN)
          .map(_ * 69)
          .flatMap { offset =>
            Seq(FoundSubstring("ligdkm", offset + 1), FoundSubstring("lidgkm", offset + 57))
          }
      )
  }

  "Очень длинная подстрока для поиска" ignore {
    val complexityN = 5000

    val sbSubstring = new StringBuilder
    (0 until complexityN) foreach { _ =>
      sbSubstring.append("ligdkm")
    }
    val substring = sbSubstring.toString

    val permutations = substring.permutations
    val sub1 = permutations.next()
    val sub2 = permutations.next()

    val sb = new StringBuilder
    sb.append("f")
    sb.append(sub1)
    sb.append("tmgdldkgkkfjsivjoqqxasoskvprfejrgkrlidgkdssadkudfg")
    sb.append(sub2)
    sb.append("ashdua")

    finder.find(sb.toString, substring).futureValue(longTimeout) should
      be(Seq(FoundSubstring(sub1, 1), FoundSubstring(sub2, 1 + (complexityN * 6) + 50)))
  }
}
