import scala.concurrent.Future

case class FoundSubstring(substring: String, count: Int)

trait Finder {
  
  def find(string: String, substring: String): Future[Seq[FoundSubstring]]

}

object Finder {

  def fast: Finder = ???

  def short: Finder = ???

  def parallel: Finder = ???

}
