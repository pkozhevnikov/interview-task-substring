This / version := "0.0.1-SNAPSHOT"
This / scalaVersion := "2.13.10"
This / scalacOptions ++= Seq("-deprecation")

lazy val root = (project in file("."))
  .settings(
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % "3.2.15" % Test
    )
  )
